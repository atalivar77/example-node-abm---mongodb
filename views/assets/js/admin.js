//global variables
var dataUser, datTable;
var modalDel, modalModif;

window.onload = function () {
    init();
}

function init() {
    //functions common btns
    btnLinks();
    //Get load data
    Events.getDataUser(loadVisualization);
}
/* BUTTONS */
function btnLinks() {
    //start
    var aStart = document.querySelector('#admin-start');
    //ABM
    var aABM = document.querySelector('#admin-abm');
    //Events
    var aTable = document.querySelector('#admin-table');
    //exit
    var aExit = document.querySelector('#btn-exit-app');

    //listeners
    aStart.addEventListener('click', function () {
        Events.admin(localStorage.token, '/exampleNode/start');
    })

    aABM.addEventListener('click', function () {
        Events.admin(localStorage.token, '/exampleNode/abm');
    })

    aTable.addEventListener('click', function () {
        Events.admin(localStorage.token, '/exampleNode/table');
    })

    aExit.addEventListener('click', function () {
        exitApp();
    });

    //load functions by active
    if (aABM.className === 'active') ABM();
    if (aTable.className === 'active') demoTable();
}
/* EXTIT APLICATION*/ 
function exitApp() {
    //remov key
    localStorage.removeItem('token');
    location.href = '/exampleNode/index';
}
//Funcion que decide que comportamiento segun  que pagina
function loadVisualization() {
    var displayName = document.querySelector('#user-name');    
    displayName.innerHTML = dataUser.name;
}


//Functions ABM
function ABM() {
    //load table
    Events.getAllProducts(renderTable);

    //inputs selectors
    const name = document.querySelector('#abmName'),
    category = document.querySelector('#abmCategory'),
    price = document.querySelector('#abmPrice');
    //btn Selector
    const btnProduct = document.querySelector('#btn_upload_product');
    //btn function
    btnProduct.addEventListener('click', function (event) {
        event.preventDefault();
        event.stopPropagation();

        if (name.value === '' || price.value === '') {
            Events.addErrorMessage('Debe complentar todos los campos');
        } else {

            $.post('/exampleNode/createProduct', { name:name.value, category: category.value, price: price.value, user: dataUser.email })
            .done(function (json) {
                var status = json.status;
                if (status === true) {
                    name.value = '';
                    price.value = '';
                    //update data table
                    Events.getAllProducts(Events.updateTable);
                    Events.addSuccessMessage(json.msg);
                } else {
                    Events.addErrorMessage(json.msg);
                }       
            }).fail(function (error) {
                Events.addErrorMessage('Error de conexión');
            })
        }
    });

    function renderTable(products) {
        //create modals
        modalModif = Events.createModalUpdate();
        modalDel = Events.createModalDelete();
        
        var tableProducts = $('#demo-table').DataTable({
            language: Events.languageTable(),
            data: products,
            columns: [
                { "title": "Nombre" , "data": 'name', "orderable": false},
                { "title": "Categoria" , "data": 'category', "orderable": false},
                { "title": "Precio" , "data": 'price', "orderable": false},
                { "title": "Creado por", "data": 'user', "orderable": false},
                { "title": "Acciones", "data":'actions', "orderable": false}
            ], 
        });

        tableProducts.on('click', 'i', function (event) {
            var self = this;
            var btnClass = self.className;
            var dataRow = tableProducts.row($(this).parents('tr')).data();
    
            event.stopPropagation(); //stop Propagation
            event.preventDefault();  //cancel event default
            
            switch (btnClass) {
                case 'fa fa-pencil-square-o':
                    var id = dataRow._id;
                    modalModif.id = id;
                    const nameE = document.querySelector('#abmNameEdit'),
                    categoryE = document.querySelector('#abmCategoryEdit'),
                    priceE = document.querySelector('#abmPriceEdit');
                    //load values
                    nameE.value= dataRow.name;
                    //categoryE.selectedIndex = ;
                    priceE.value = dataRow.price;
                    modalModif.open();

                    break;
                case 'fa fa-trash':
                    var id = dataRow._id;
                    var text = document.querySelector('#modal_del');
                    text.innerHTML = '¿Esta seguro que desea eliminar el producto ' +  dataRow.name + ' ?';
                    modalDel.id = id;
                    modalDel.open();
                    break
                default:
                    break;
            }
            
        });
        datTable = tableProducts;
    }    
}        

//Function demoTable
function demoTable() {
    //load data
    $.getJSON( "../assets/js/list.json", function( data ) {
        renderTable(data);
        
    },function (error) {
        Events.addErrorMessage('No se pudo conectar, intententarlo mas tarde');
    });
     
    function renderTable(response) {
        var employes =  response.data;
        
        var tablePromotions = $('#demo-table').DataTable({
            language: Events.languageTable(),
            data: employes,
            columns: [
                { "title": "Nombre" , "orderable": true},
                { "title": "Puesto" , "orderable": true},
                { "title": "Oficina" , "orderable": true},
                { "title": "Interno",  "orderable": true},
                { "title": "Fecha de inicio" , "orderable": true},
                { "title": "Salario ", "orderable": false}
            ]
        });
    } 
}
   