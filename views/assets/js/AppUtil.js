/*! AppUtil - v0.1.0 - (c) 2018 Martin Balmaceda - !*/

if(typeof AppUtil === 'undefined') { 
	AppUtil = {};
}

(function (AppUtil) {

	"use strict";

	var Element = (function () {
		// private variables
		var content, child;

		// contruct
		function Element(typeElement, parent, id, css) {
			this.parent = parent; //object dom parent
			this.element = document.createElement(typeElement); // create a new dom element
			if(id !== null && id !== undefined) { this.element.setAttribute('id', id); }
			if(css !== null && css !== undefined) { this.element.setAttribute('class', css); }
			this.parent.appendChild(this.element);
		}

		// set css Class
		Element.prototype.setClass = function (css) {
			this.element.css = css;
		};

		// add addAtributte dom
		Element.prototype.addAtributte = function (type, value) {
			this.element.setAttribute(type, value);
		};

		// add event
		Element.prototype.addEvent = function (type, callback) {
			this.element.addEventListener(type, callback);
		};

		// remove Event
		Element.prototype.removeEvent = function (type, callback) {
			this.element.removeEventListener(type, callback);
		};

		// add events
		Element.prototype.addEvents = function (type, callback) {
			var tmp = type.split(" "), self = this;
			//register all events
			if (tmp.length >1) {
				for (var i = 0; i < tmp.length; i++) {
					this.element.addEventListener(tmp[i], callback);
				}
			} else if ( tmp.length == 1) { //register one event
				self.element.addEventListener(tmp[0], callback);
			}
		};

		// remove events
		Element.prototype.removeEvents = function (type, callback) {
			var tmp = type.split(" "), self = this;
			//register all events
			if (tmp.length >1) {
				for (var i = 0; i < tmp.length; i++) {
					this.element.removeEventListener(tmp[i], callback);
				}
			} else if ( tmp.length == 1) { //register one event
				self.element.removeEventListener(tmp[0], callback);
			}
		};

		// add text
		Element.prototype.addText = function (text) {
			this.element.innerText = text;
		};

		// get value
		Element.prototype.getValue = function () {
			return this.element.value;
		};

		// add Value
		Element.prototype.addValue = function (text) {
			this.element.setAttribute('value', text);
		};

		// add Name
		Element.prototype.addName = function (text) {
			this.element.setAttribute('name', text);
		};

		// remove element
		Element.prototype.removeElement = function () { // remove element
			this.parent.removeChild(this.element);
		};

		// add Parent
		Element.prototype.addDivParent = function () {
			var divContent = new Element('div', this.parent, 'content-' + this.element.id, '');
			divContent.element.appendChild(this.element);
			this.parent  = divContent.element;
			this.content = divContent; // this line add access to element
		};

		// add child
		Element.prototype.addChild = function (type) {
			var child = new Element(type, this.parent, type+'-' + this.element.id, '');
			this.element.appendChild(child.element);
			this.child = child;//this line add access to element
		};

		// add class
		Element.prototype.addClass = function (css) {
			var value = this.element.className;
			this.element.setAttribute('class', value +' '+ css);
		};

		// delete a class
		Element.prototype.removeClass = function (css) {
			var value = this.element.className; // the main class
			var tmp   = value.split(" "), self = this; //transform string to array
			var position = tmp.indexOf(css); //find the posicition
	
			tmp.splice(position, 1); //delete css
			value = tmp.toString(); //convert again in the string
			value = value.replace(/,/g,' '); //delete special char
			this.element.setAttribute('class', value);
		};

		// move to first child
		Element.prototype.insertBefore = function (element) {
			if(this.element == element) {
				return;
			}
			this.removeElement();
			this.parent.insertBefore(this.element, element);
		};

		return Element; //return Element class

	}()); //Has no inheritance

	// module/export
  	AppUtil.Element = Element;

})(AppUtil || (AppUtil = {}));


(function (AppUtil) {

	"use strict";
	
	function addClass(element, classes) {
		var fnCallback = function(cssClass) {
			element.classList.add(cssClass);
		};

		if (isArray(classes)) {
			classes.forEach(function(strClass) { strClass.split(" ").forEach(fnCallback); });
		} else {
			classes.split(" ").forEach(fnCallback);
		}
	}
	
	function removeClass(element, classes) {
		var fnCallback = function(cssClass) {
			element.classList.remove(cssClass);
		};

		if (isArray(classes)) {
			classes.forEach(function(strClass) { strClass.split(" ").forEach(fnCallback); });
		} else {
			classes.split(" ").forEach(fnCallback);
		}
	}
	
	// module/export
	AppUtil.addClass = addClass;
	AppUtil.removeClass = removeClass;
	
})(AppUtil || (AppUtil = {}));


(function (AppUtil) {

	"use strict";

	function setHeader(xhr, requestHeader) {
		Object.keys(requestHeader).forEach(function(option){
			xhr.setRequestHeader(option, requestHeader[option]);
		});
	}

	function formatObject(sendData) {
		// create formData
		var formData = new FormData();
		// append all data
		Object.keys(sendData).forEach(function(data){
			formData.append(data, sendData[data]);
		});
		// return
		return formData;
	}

	function reqFromServer(method, url, sendData, responseType, requestHeader) {
		// return request promise
		return new Promise(function (resolve, reject) {
			// object XML Htt Request
			var xhr = new XMLHttpRequest();
			// load the controller page
			xhr.open(method, url);
			// send the proper header information along with the request
			if(requestHeader !== undefined && requestHeader !== null) {
				setHeader(xhr, requestHeader);
			}
			// set response type
			if(responseType !== undefined && responseType !== null) {
				xhr.responseType =  responseType;
			}
			// check request state
			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4) {	
					if (xhr.status === 200) {
						xhr.onload = function () {
							resolve(xhr.response);
						};
						xhr.onerror = function () {
							reject({
								status: this.status,
								statusText: xhr.statusText
							});
	          			};
					} else {
	              		reject({
							status: this.status,
							statusText: xhr.statusText
						});
					}
				}
			};
         	// no send data for a response from the server
			if(typeof sendData === typeof undefined || sendData === null){
				xhr.send();
			} else {
				xhr.send(formatObject(sendData));
			}
		});
	};

	// module/export
	AppUtil.reqFromServer = reqFromServer;

	var requestFromServer = function(method, url, sendData){
		return new Promise(function (resolve, reject) {
		  var xhr = new XMLHttpRequest(); //object XML Htt Request
			  xhr.open(method, url); // load the controller page
			  xhr.onload = function () {
				if (this.status >= 200 && this.status < 300) {
				  resolve(xhr);  //success Result!
				} else {
				  reject({
					status: this.status, //fail! of the transaction
					statusText: xhr.statusText
				  });
				}
			  };
			  xhr.onerror = function () {
				reject({
				  status: this.status,
				  statusText: xhr.statusText
				});
			  };
			  if(typeof sendData === typeof undefined){
				xhr.send(); // no send data for a response from the server
			  } else {
				xhr.send(sendData); //send data
			  }
		});
	}
	AppUtil.requestFromServer = requestFromServer;

	function removeAllChild (parent) {
		if (parent.childNodes[0]) { //exist a children?
			var rangeObj = new Range();
			// Select all of theParent's children
		  	rangeObj.selectNodeContents(parent);
	
		  	// Delete everything that is selected
		  	rangeObj.deleteContents();
		}
	}
	AppUtil.removeAllChild = removeAllChild;
	
	function removeSelf (el) {
		el.parentNode.removeChild(el);// Remove Element
	}
	AppUtil.removeSelf = removeSelf;

	function isOnline() {
		if (navigator.onLine) {
			return true;
		} else {
			return false;
		}
	}
	AppUtil.isOnline = isOnline;

	function device() {		
		if( navigator.userAgent.match(/Android/i)
			|| navigator.userAgent.match(/webOS/i)
			|| navigator.userAgent.match(/iPhone/i)
			|| navigator.userAgent.match(/iPad/i)
			|| navigator.userAgent.match(/iPod/i)
			|| navigator.userAgent.match(/BlackBerry/i)
			|| navigator.userAgent.match(/Windows Phone/i)
		){
			return true; // its a mobil device
		} else { 
			return false;} //its a desktop
		}
	AppUtil.device = device;

	//validations
	function vNumber(e){
		var key = window.Event ? e.which : e.keyCode
		var finalK;
	}
	AppUtil.vNumber = vNumber;

})(AppUtil || (AppUtil = {}));

//brower functions
(function (AppUtil) {
	"use strict";
	
	function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch(e) {
            return e instanceof DOMException && (
                // everything except Firefox
                e.code === 22 ||
                // Firefox
                e.code === 1014 ||
                // test name field too, because code might not be present
                // everything except Firefox
                e.name === 'QuotaExceededError' ||
                // Firefox
                e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                storage.length !== 0;
        }
    }

	function getBrowser(){
		var ua = navigator.userAgent, tem,
		M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		if(/trident/i.test(M[1])){
			tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
			return 'IE '+(tem[1] || '');
		}
		if(M[1]=== 'Chrome'){
			tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
			if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
		}
		M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
		if((tem= ua.match(/version\/(\d+)/i))!= null) 
			M.splice(1, 1, tem[1]);
		return M.join(' ');
	}
	
	var isDevice = function () {
		var localThis = this;
		var deviceType = {};
		var types = [/Android/i,/webOS/i,/iPhone/i,/iPad/i,/iPod/i,/BlackBerry/i,/Windows Phone/i];

		deviceType.browserVer = localThis.getBrowser();
		deviceType.browser = deviceType.browserVer.split(" ")[0];

		for (var i = 0; i < types.length; i++) {
			if (navigator.userAgent.match(types[i])) {
				deviceType.mode = "mobile";
				
				switch (types[i]) {
					case '/Android/i':
						deviceType.system = 'android';
						break;
					case '/webOS/i':
						deviceType.system = 'webOS';
						break;
					case '/iPhone/i':
						deviceType.system = 'iPhone';
						break;
					case '/iPad/i':
						deviceType.system = 'iPad';
						break;		
					case '/iPod/i':
						deviceType.system = 'iPod';
						break;
					case '/BlackBerry/i':
						deviceType.system = 'BlackBerry';
						break;
					case '/Windows Phone/i':
						deviceType.system = 'Windows Phone';
						break;
					return deviceType;			
				}
			} else {
				deviceType.mode = "desk";
				return deviceType;	
			}
		}
	}

	//Module Export
	AppUtil.storageAvailable = storageAvailable;
	AppUtil.getBrowser = getBrowser;
	AppUtil.isDevice = isDevice;
	
})(AppUtil || (AppUtil = {}));