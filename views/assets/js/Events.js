var Events;

(function (Events) {

    //authentification conection
    function authentification(url, token, controller) {
        const myHeaders = new Headers();
        myHeaders.append('authorization', `Bearer ${token}`);
        myHeaders.append('page', url);
        myHeaders.append('Accept', 'application/json');
        myHeaders.append('Content-Type', 'application/json');

        fetch('authorization', {
            method: 'post',
            headers: myHeaders
        })
            .then(res => (console.log('Bienvenido')))
    }
    Events.authentification = authentification;

    function admin(token, url) {
        var form = document.createElement("form");
        form.setAttribute("method", "get");
        form.setAttribute("action", url);
        form.setAttribute('Accept', 'application/json');
        form.setAttribute("enctype", "application/x-www-form-urlencoded");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute('name', 'authorization');
        hiddenField.setAttribute("value", `Bearer ${token}`);
        form.appendChild(hiddenField);
        //form.setAttribute("target", targetName);
        document.body.appendChild(form);
        // ejecuto el submit
        form.submit();

    }
    Events.admin = admin;

    function getDataUser(callback) {
        var data = { token: localStorage.token };

        $.post('/exampleNode/getUserData', data)
            .done(function (response) {
                dataUser = response;
                callback();
            }).fail(function (error) {
                console.log(error);
            })
    }
    Events.getDataUser = getDataUser;

    //Messages
    function addSuccessMessage(text) {
        iziToast.success({
            title: 'Éxito',
            message: text,
            position: 'topCenter',
        });

    }
    Events.addSuccessMessage = addSuccessMessage;

    function addErrorMessage(text) {
        iziToast.error({
            title: 'Error',
            message: text,
            position: 'topCenter',
        });

    }
    Events.addErrorMessage = addErrorMessage;

    //storage
    function loadLocalStorage(key) {
        localStorage.getItem(key);
    }
    Events.loadLocalStorage = loadLocalStorage;

    function saveInLocalStorage(keyValue) {
        //verification local storage
        if (AppUtil.storageAvailable('localStorage')) {
            if (!localStorage.getItem(keyValue.key)) {
                localStorage.removeItem(keyValue.key);
            }
            localStorage.setItem(keyValue.key, keyValue.value);
        } else {
            addErrorMessage('Tu navegador es muy Antiguo, prueba con versiones mas modernas');
        }
    }
    Events.saveInLocalStorage = saveInLocalStorage;

    //Products
    function getAllProducts(callback) {
        $.post('/exampleNode/getProducts')
            .done(function (response) {                
                if (response.products) callback(Events.ordenableData(response.products));
            }).fail(function (error) {
                console.log(error);
            })
    }
    Events.getAllProducts = getAllProducts;

    //delete product
    function deleteProduct() {
        const id = modalDel.id;

        $.post('/exampleNode/deleteProduct', {id: id })
            .done(function (json) {
                if (json.status === false) {
                    Events.addErrorMessage(json.msg);
                } else {
                    Events.addSuccessMessage(json.msg);
                    var table = dataUser.datatble;
                    Events.getAllProducts(Events.updateTable);
                }
            }).fail(function (error) {
                Events.addErrorMessage('Error de conexión');
            })
    }
    Events.deleteProduct = deleteProduct;


    //table functions
    function updateTable(response) {
        var data = response;
        var datatable = datTable;
        //update table
        datatable.clear();
        datatable.rows.add(data).draw('false');
    }
    Events.updateTable = updateTable;

    function ordenableData(products) {
        products.forEach(product => {
            product.actions = '<i data-id="' + product._id + '" class="fa fa-pencil-square-o" aria-hidden="true" style="cursor: pointer"></i>' +
            '<i data-id="' + product._id + '" class="fa fa-trash" aria-hidden="true" style="cursor: pointer"></i>';
        });
        return products;
    }
    Events.ordenableData = ordenableData;

    function languageTable() {
        const language = {
            "decimal": "",
            "emptyTable": "No hay ningún elemento cargado",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
        return language;
    }
    Events.languageTable = languageTable;

    //Modals
    function createModalUpdate() {
        // instanciate new modal
        var modal = new tingle.modal({
            footer: true,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            cssClass: ['custom-class-1', 'custom-class-2'],
            beforeClose: function () {
                return true; // close the modal
            }
        });

        // set content
        modal.setContent(            
            '<div class="panel-body">'+
            '<label style="width:33%" >'+
                '<p class="panel-subtitle" style="margin-bottom: 10px">Nombre</p>'+
                '<input id="abmNameEdit" class="form-control" placeholder="Por ejemplo "Kiwi"" type="text">'+
				'</label>'+
                '<label style="width:33%" >'+
                    '<p class="panel-subtitle" style="margin-bottom: 10px">Categoría</p>'+
                    '<select class="form-control" name="selec" id="abmCategoryEdit">'+
                        '<option value="carnes rojas">carnes rojas</option>'+
                        '<option value="pastas">pastas</option>'+
                        '<option value="frutas">frutas</option>'+
                        '<option value="verduras">verduras</option>'+
                        '<option value="pescado">pescado</option>'+
                    '</select>'+
                '</label>'+
                '<label style="width:33%" >'+
                '<p class="panel-subtitle" style="margin-bottom: 10px">Precio</p>'+
                '<input id="abmPriceEdit" class="form-control" placeholder="17.00" type="number">'+
                '</label>'+
                '</div>'
            );
    
        modal.addFooterBtn('Actualizar', 'tingle-btn tingle-btn--danger', function () {
            Events.updateProduct();
            modal.close();
        });

        modal.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--default', function () {
            modal.close();
        });

        return modal;
    }
    Events.createModalUpdate = createModalUpdate;

    //modal
    function createModalDelete() {
        // instanciate new modal
        var modal = new tingle.modal({
            footer: true,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            cssClass: ['custom-class-1', 'custom-class-2'],
            beforeClose: function () {
                return true; // close the modal
            }
        });

        // set content
        modal.setContent('<h4 id="modal_del" ></h4>');

        // add a button
        modal.addFooterBtn('Borrar', 'tingle-btn tingle-btn--danger', function () {
            deleteProduct();
            modal.close();
        });

        modal.addFooterBtn('Cancelar', 'tingle-btn tingle-btn--default', function () {
            modal.close();
        });

        return modal;
    }
    Events.createModalDelete = createModalDelete;

    

   
    function updateProduct() {
        var id = modalModif.id;
        const nameE = document.querySelector('#abmNameEdit'),
        categoryE = document.querySelector('#abmCategoryEdit'),
        priceE = document.querySelector('#abmPriceEdit');

        if (nameE.value === '' || priceE.value === '' ) {
            Events.addErrorMessage('Debes completar todos los campos.')
        } else {
            $.post('/exampleNode/updateProduct', {id: id, name: nameE.value, category: categoryE.value, price: priceE.value, user: dataUser.email})
            .done(function (json) {
                if (json.status === false) {
                    Events.addErrorMessage(json.msg);
                } else {
                    if (json.msg) {
                        Events.addSuccessMessage(json.msg);
                        var table = dataUser.datatble;
                        Events.getAllProducts(Events.updateTable);
                    } else {
                        Events.addErrorMessage('Error al actualizar.');
                    }
                }
            }).fail(function (error) {
                Events.addErrorMessage('Error de conexión');
            })
        }
    }
    Events.updateProduct = updateProduct;

}(Events || (Events = {})))