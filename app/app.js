//import node modules
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
//const multer = require('multer');
const formData = require('express-form-data');
const path = require('path');

//definicion de la constante express
const app = express();
const router = require('./router/index');

//enables cors
app.use(cors({
    'allowedHeaders': ['sessionId', 'Content-Type'],
    'exposedHeaders': ['sessionId'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
}));

//use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//dir estatica base
const baseUrl = require('./../config/config').baseUrl;

//elementos estaticos
var publicPath = path.resolve(baseUrl, 'views'); 
var resources = path.resolve(baseUrl, 'resources');

// Para que los archivos estaticos queden disponibles.
app.use(express.static(publicPath));
app.use(express.static(resources));

// routes ======================================================================
app.use('/exampleNode',router);

module.exports = app;