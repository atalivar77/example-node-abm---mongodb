'use strict'

const services = require('../services/index');
const url = require('url');


function isAuth(req, res, next) {
    //get param from URL
    var authorization = req.query.authorization;

    if (!authorization) {
        return res.status(403).send({ mesagge: 'No tienes autorización' })
        //res.redirect('/promotions/index');
    }

    const token = authorization.split(" ")[1];

    services.decodeToken(token)
        .then((response) => {
            req.user = response;
            return next();
        })
        .catch((response) => {
            res.status(response.status).send({ message: "response.message" });
        })
}


module.exports = {
    isAuth
};